#!/bin/sh

validTypes=("basic" "advanced")

selectedType=$1

validType=1
 
for type in ${validTypes[@]}
do
   if [ "$selectedType" == "$type" ]
    then
        echo "found"
        validType=0
        break
    fi
done

if [ $validType -ne 0 ]
then
    echo "Invalid argument defined. You must choose from the below:"

    for type in ${validTypes[@]}
    do
        echo "$type"
    done

    echo "E.g. install_vimrc ${validTypes[0]}"
 
    exit 1
fi

targetFile=""

which vi >& /dev/null
if [ $? -eq 0 ]
then
    targetFile=".virc"
fi

which vim &> /dev/null
if [ $? -eq 0 ]
then 
    targetFile=".vimrc"
else
    if [ targetFile != "" ] && [ selectedType == "basic" ]
    then
        echo "vImproved must be installed to use other configs than \"basic\""
	exit 1
    fi
fi

if [ "$targetFile" == "" ]
then
    echo "No compatible editor detected..."
    exit 1
fi

# Need to find a way to work out if symlink OR file
if [ -e "$HOME/$targetFile" ]
then
    echo "Existing ~/$targetFile found..."

    counter=1
    bkpFile="$targetFile-bkp-$counter"

    while [ $counter -lt 5 ]
    do
        if [ -e "$HOME/$bkpFile" ]
        then
            ((counter++))
            bkpFile="$targetFile-bkp-$counter"
        else
            break
        fi
    done

    if [ $counter -eq 5 ]
    then
        echo "There are 4 existing backups, please remove one and try again"
        exit 1
    fi

    $(mv "$HOME/$targetFile" "$HOME/$bkpFile")
    if [ $? -eq 0 ]
    then
        echo "Sucessfully backed up file to ~/$bkpFile..."
    else
        echo "Failed to backup file to ~/$bkpFile"
        exit 1
    fi
else
    echo -e "No existing ~/$targetFile found..."
fi

$(ln -s "$(cd ../ && pwd)/vimrcs/$selectedType.vim" "$HOME/$targetFile")
if [ $? -eq 0 ]
then
    echo "Symlink successful..."
else
    echo "Failed to make symlink to ~/$targetFile"
    exit 1
fi
if [ "$selectedType" == "basic" ]
then
    echo "You're all setup, enjoy!"
    exit 0
fi

if [ -e "$HOME/.vim" ]
then
    read -p "Detected ~/.vim is not empty and will delete this. Are you sure you want to continue? <y/N> " prompt
    if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]]
    then
        $(rm -rf "$HOME/.vim")
        if [ $? -eq 0 ]
        then
            echo "Successfully removed ~/.vim..."
        else
            echo "Failed to remove ~/.vim/bundle"
            exit 1
        fi
    else
        echo "You must allow vundle to control your ~/.vim folder."
        exit 1
    fi
else
    echo "No ~/.vim directory found..."
fi

$(mkdir -p "$HOME/.vim/bundle")
if [ $? -eq 0 ]
then
    echo "Successfully created ~/.vim..."
else
    echo "Failed to create ~/.vim/bundle"
    exit 1
fi

$(git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim)
if [ $? -eq 0 ]
then
    echo  "Successfully cloned vundle..."
else
    echo "Failed to clone vundle"
    exit 1
fi

echo "Installing vundle plugins..."

vim +PluginInstall +qall --not-a-term &> /dev/null
if [ $? -eq 0 ]
then
    echo "Successfully installed vundle plugins..."
else
    echo "Failed to install vundle plugins"
    exit 1
fi

echo "You're all setup, enjoy!"

