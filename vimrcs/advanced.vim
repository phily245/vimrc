" Maintainer: 
"       Phil Young
"       phily245@gmail.com
"
" Version: 
"       0.1 - 30/01/2018 
"
" Sections:
"    -> Include Basic
"    -> Vundle Init
"    -> Vundle Plugins
"    -> Vundle Close
"    -> Colours and Fonts
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Basic
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 if filereadable(expand('~/.vimrc'))
    let vimrcFolder = split(resolve(expand('~/.vimrc')), '/')
    :execute 'source /' . join(vimrcFolder[0:-2], '/') . '/basic.vim'
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vundle Init
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Required by Vundle, be iMproved
set nocompatible

" Required by vundle
filetype off

" Set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vundle Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The Dracula colour theme
Plugin 'dracula/vim'

" Cool Status/tabline
Plugin 'vim-airline/vim-airline'

set  rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vundle Close
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Required by vundle after all plugins loaded
call vundle#end()
filetype plugin indent on

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colours and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
color dracula

